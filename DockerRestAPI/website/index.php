<html>
    <head>
        <title>CIS 322 Rest-api: Control Times</title>
    </head>

    <body>
        <h1>Control Times</h1>
        <form method="post">
            <input type="submit" name="button1" value="ListAll"/>
            <input type="submit" name="button2" value="ListOpen"/>
            <input type="submit" name="button3" value="ListClose"/>
            <input type="submit" name="button4" value="ListAllCSV"/>
            <input type="submit" name="button5" value="ListOpenCSV"/>
            <input type="submit" name="button6" value="ListCloseCSV"/>
        </form>

        <?php
          if(isset($_POST['button1'])) {
            $json = file_get_contents('http://web-service/listAll');
            $obj = json_decode($json);
              $controls = $obj->Controls;
            foreach ($controls as $control) {
                echo "<li>$control</li>";
            }
          }

          if(isset($_POST['button2'])) {
            $json = file_get_contents('http://web-service/listOpenOnly');
            $obj = json_decode($json);
              $controls = $obj->Controls;
            foreach ($controls as $control) {
                echo "<li>$control</li>";
            }
          }

          if(isset($_POST['button3'])) {
            $json = file_get_contents('http://web-service/listCloseOnly');
            $obj = json_decode($json);
              $controls = $obj->Controls;
            foreach ($controls as $control) {
                echo "<li>$control</li>";
            }
          }

          if(isset($_POST['button4'])) {
            $json = file_get_contents('http://web-service/listAll/csv');
            $obj = json_decode($json);
            echo nl2br($obj);
          }

          if(isset($_POST['button5'])) {
            $json = file_get_contents('http://web-service/listOpenOnly/csv');
            $obj = json_decode($json);
            echo nl2br($obj);
          }

          if(isset($_POST['button6'])) {
            $json = file_get_contents('http://web-service/listCloseOnly/csv');
            $obj = json_decode($json);
            echo nl2br($obj);
          }
        ?>

    </body>

</html>
