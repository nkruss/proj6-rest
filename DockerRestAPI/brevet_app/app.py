import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
from pymongo import MongoClient

import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

client = MongoClient("db", 27017)

#reset the tododb database
client.drop_database('tododb')
db = client['tododb']

app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

@app.route('/')
def todo():
    return render_template('todo.html')

@app.route('/new')
def new():
    control_dist = request.args.get('km', 1e5, type=float)
    open_time = request.args.get('open_time')
    close_time = request.args.get('close_time')
    dist = request.args.get('distance', 200, type=float)
    start_time = request.args.get('start_time')

    item_doc = {
        'brevet_dist': dist,
        'start_time': start_time,
        'control_dist': control_dist,
        'open': open_time,
        'close': close_time
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

@app.route('/display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('display.html', items=items)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 1e5, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    dist = request.args.get('distance', 200, type=float)
    start_time = request.args.get('start_time')

    print("km ", km)
    print("dist ", dist)
    print("start_time ", start_time)
    brevet_start_time = arrow.get(start_time)
    brevet_start_time = brevet_start_time.shift(hours=+8).isoformat()

    open_time = acp_times.open_time(km, dist, brevet_start_time)
    close_time = acp_times.close_time(km, dist, brevet_start_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

class ListAll(Resource):
    def get(self):
        _items = db.tododb.find({}, {'control_dist': 1, 'open': 1, 'close': 1, '_id': 0}).sort([('control_dist', 1)])
        items = [item for item in _items]
        for i in range(len(items)):
            control_dist = str(items[i]['control_dist'])
            open_time = str(items[i]['open'])
            close_time = str(items[i]['close'])
            items[i] = f"control: {control_dist}, open: {open_time}, close: {close_time}"

        top = request.args.get("top")
        if top == None:
            return {"Controls": items}
        else:
            top = min(int(top), len(items))
            return {"Controls": items[:top]}

class ListOpen(Resource):
    def get(self):
        _items = db.tododb.find({}, {'control_dist': 1, 'open': 1, 'close': 1, '_id': 0}).sort([('control_dist', 1)])
        items = [item for item in _items]
        for i in range(len(items)):
            control_dist = str(items[i]['control_dist'])
            open_time = str(items[i]['open'])
            items[i] = f"control: {control_dist}, open: {open_time}"

        top = request.args.get("top")
        if top == None:
            return {"Controls": items}
        else:
            top = min(int(top), len(items))
            return {"Controls": items[:top]}

class ListClose(Resource):
    def get(self):
        _items = db.tododb.find({}, {'control_dist': 1, 'open': 1, 'close': 1, '_id': 0}).sort([('control_dist', 1)])
        items = [item for item in _items]
        for i in range(len(items)):
            control_dist = str(items[i]['control_dist'])
            close_time = str(items[i]['close'])
            items[i] = f"control: {control_dist}, close: {close_time}"

        top = request.args.get("top")
        if top == None:
            return {"Controls": items}
        else:
            top = min(int(top), len(items))
            return {"Controls": items[:top]}

class ListAllCSV(Resource):
    def get(self):
        _items = db.tododb.find({}, {'control_dist': 1, 'open': 1, 'close': 1, '_id': 0}).sort([('control_dist', 1)])
        items = [item for item in _items]
        control_list = []
        open_list = []
        close_list = []

        top = request.args.get("top")
        if top == None:
            top = len(items)
        else:
            top = min(int(top), len(items))

        for i in range(top):
            control_list.append(str(items[i]['control_dist']))
            open_list.append(str(items[i]['open']))
            close_list.append(str(items[i]['close']))

        csv_string = "control, open, close \n"
        for i in range(len(control_list)):
            csv_string += control_list[i] + ', '
            csv_string += open_list[i] + ', '
            csv_string += close_list[i] + '\n'

        return csv_string

class ListOpenCSV(Resource):
    def get(self):
        _items = db.tododb.find({}, {'control_dist': 1, 'open': 1, 'close': 1, '_id': 0}).sort([('control_dist', 1)])
        items = [item for item in _items]
        control_list = []
        open_list = []

        top = request.args.get("top")
        if top == None:
            top = len(items)
        else:
            top = min(int(top), len(items))

        for i in range(top):
            control_list.append(str(items[i]['control_dist']))
            open_list.append(str(items[i]['open']))

        csv_string = "control, open \n"
        for i in range(len(control_list)):
            csv_string += control_list[i] + ', '
            csv_string += open_list[i] + '\n'

        return csv_string

class ListCloseCSV(Resource):
    def get(self):
        _items = db.tododb.find({}, {'control_dist': 1, 'open': 1, 'close': 1, '_id': 0}).sort([('control_dist', 1)])
        items = [item for item in _items]
        control_list = []
        close_list = []

        top = request.args.get("top")
        if top == None:
            top = len(items)
        else:
            top = min(int(top), len(items))

        for i in range(top):
            control_list.append(str(items[i]['control_dist']))
            close_list.append(str(items[i]['close']))

        csv_string = "control, close \n"
        for i in range(len(control_list)):
            csv_string += control_list[i] + ', '
            csv_string += close_list[i] + '\n'

        return csv_string



# Create routes
# Another way, without decorators
api.add_resource(ListAll, '/listAll', '/listAll/json')
api.add_resource(ListOpen, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(ListClose, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(ListAllCSV, '/listAll/csv')
api.add_resource(ListOpenCSV, '/listOpenOnly/csv')
api.add_resource(ListCloseCSV, '/listCloseOnly/csv')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
